import { Component, OnInit } from '@angular/core';



@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  team1: string[] = ['ajax', 'feijnoord', 'psv', 'barcelona', 'real madrid', 'valencia', 'juventus'];
  team2: string[] = ['rodajc', 'helmond sport', 'sparta', 'mexico-city', 'club brugge', 'rode tijgers', 'apen'];
  regels: string[] = [];
  standen: string[] = [];


  ngOnInit() {

    for (let i in this.team1) {
      this.regels.push(this.team1[i] + "-" + this.team2[i]);
    }

    for (let i in this.team1) {
      this.standen.push(Math.round(Math.random() * 2) + "-" + Math.round(Math.random() * 2));
    }
  }

  getCompetitieregel(i: number): string {

    return this.regels[i];

  }

  getStand(i: number) :string {
    return this.standen[i];
  }
}
